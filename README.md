<h1>Personal Task Monitor for terminal</h1>

<p align="center">
  <img src="finalpresentation.png" alt="Header custom image"/>
</p>

#                                 Welcome to Personal Task Manager code

It is a simple monitor for personal tasks executable in terminal. <mark>Recommended to use with Byobu and if you have to manage several servers at the same time. </mark>

Aclaration it is not the best implementation, but when I started to code, I had serious problems of time. I had to solve a really hot fix in production and my chief at the same time needed a human tasks manager in terminal. I had to do two tasks at the same time. Code is functional and run using python 3.8.2.

<mark>BUT IS NOT VERY PYTHONIC, and the asyncio implementation is not very good, is needed to do some refactor. BUT CODE RUN AND THE FAST TEST shows no memory compromises. </mark>

If you are using ubuntu or other linux distro, ensure you have codecs to reproduce mp3. This monitor play sounds. 

Run commands: <br />**sudo apt-get install libasound2-dev**<br />**sudo apt-get install vlc**

1: Ensure python >= 3.8.2 in terminal.(Pyenv is a great idea to manage python versions)

2: Install needed packages:<br />**pip3 install colorama**<br />**pip3 install asyncio**

3: Clone the repo using git clone

4: execute **python monitor.py**

You can change the tasks and datetimes in the file [task.csv](https://gitlab.com/deBmichel/personalterminaltaskmonitor/-/raw/master/tasks.csv)

REview your terminal: You should see something similar to the first image in this repo.

Control your sound volume:

<p align="center">
  <img src="aboutsoundfinal.png" alt="Header custom image"/>
</p>


