#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         Begin Section related with GRAPHICS                               
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
from colorama import init as coloramainit, Fore, Style, Back
import sys
coloramainit()


def stringwithcolor(thestring, thecolor):
	if thecolor.upper() == "RED":
		return Fore.RED + thestring
	elif thecolor.upper() == "GREEN":
		return Fore.GREEN + thestring
	elif thecolor.upper() == "YELLOW":
		return Fore.YELLOW + thestring
	elif thecolor.upper() == "BLUE":
		return Fore.BLUE + thestring
	else:
		print("NO COLOR, USING GREEN ......")
		return Fore.GREEN + thestring

def strcoloredmapasstr(strcoloredmap):
	headerstr = ""
	for k,v in strcoloredmap.items():
		headerstr += stringwithcolor(k, v)
	return headerstr

def messageToDeleteRing():
	lineA = strcoloredmapasstr(
		{"\t\t\t\t\t\tTo STOP RING PLEASE REMOVE OR UPDATE WITH MORE TIME FINISHED TASKS !!!!!!\n" : "YELLOW", }
	)
	lineB = strcoloredmapasstr(
		{"\t\t\t\t\t\tTo STOP RING PLEASE REMOVE OR UPDATE WITH MORE TIME FINISHED TASKS !!!!!!\n" : "BLUE", }
	)
	lineC = strcoloredmapasstr(
		{"\t\t\t\t\t\tTo STOP RING PLEASE REMOVE OR UPDATE WITH MORE TIME FINISHED TASKS !!!!!!\n" : "RED", }
	)
	return lineA + lineB + lineC

def headeractivetaskmonitor():
	headerdict = {
		"v-----------:  Task  :----------v" : "YELLOW", 
		"v-----------:  date  :----------v" : "BLUE", 
		"v---:running times to finishs:---v" : "RED",
	}

	return strcoloredmapasstr(headerdict) 
                                
debianimage = [                 '''                 ________             ''' , 
                                '''             _ggDBDBDBDD"gg_          ''' ,
                                '''          .gBDBBDDBDBDBDBDBDg.        ''' ,
                                '''        ;g$$$$$$$$$$$$$$$$DDDBB.      ''' ,
                                '''       gB$$P""""       `s."$$DDD.     ''' ,
                                '''      '$$P'"'             `$$$."B     ''' ,
                                '''     '!$$P"      ,ggs.     `$$b:"     ''' ,
                                '''     Bd$$'     ,$D"'   .    $$$"!     ''' ,
                                '''     B$$B      d$'     ,    $$D.      ''' ,
                                '''     B$$:      $$.   -    ,d$$'       ''' ,
                                '''     B$$;      Y$b._   _,d$D'         ''' ,
                                '''      B$$.      `"Y$$$$D"'            ''' ,
                                '''      `$$b                            ''' ,
                                '''       `Y$$                           ''' ,
                                '''        `Y$$.                         ''' ,
                                '''          `$$b.                       ''' ,
                                '''            `Y$$b.                    ''' ,
                                '''               `"Y$b._                ''' ,
                                '''                   `"Yb._             ''' ,
                                '''                         """-         ''' ,
                                '''                               ""-- . ''' ,
]

basetoolmessage = [

          '''---------------------------------------------------------------------------------------------------- ''',
          '''-                                :  Human-Tasks monitor terminal  :                                - ''',
          '''---------------------------------------------------------------------------------------------------- ''',
          '''-:DEDICATED TO: ->  Debian GNU/Linux :x86_64  <-  ''',
          '''-       ->             CVLC             <-       -''',
          '''-       ->          CPU AMD a8          <-       -''',
          '''-       ->           NEOFETCH           <-       -''',
          '''-       ->            HUMANS            <-       -''',
          '''-       ->            PYTHON            <-       -''',
          '''-       ->             JHON             <-       -''',
          '''-       ->             CTOP             <-       -''',
          '''---------------------------------------------------------------------------------------------------- ''',
]


toolmessage = [
          stringwithcolor(basetoolmessage[0], "GREEN"  ) ,
          stringwithcolor(basetoolmessage[1], "GREEN"  ) ,
          stringwithcolor(basetoolmessage[2], "GREEN"  ) ,
          stringwithcolor(basetoolmessage[3], "YELLOW"  ) + stringwithcolor(basetoolmessage[7], "BLUE"  ) ,
          stringwithcolor(basetoolmessage[4], "YELLOW"  ) + stringwithcolor(basetoolmessage[8], "BLUE"  ) ,
          stringwithcolor(basetoolmessage[5], "YELLOW"  ) + stringwithcolor(basetoolmessage[9], "RED"  ) ,
          stringwithcolor(basetoolmessage[6], "YELLOW"  ) + stringwithcolor(basetoolmessage[10], "RED"  ) ,
          stringwithcolor(basetoolmessage[11], "GREEN"  ) ,
          
]

def especialwhiteline():
	return " " * len(debianimage[0])

def marktoreplacestr():
	return "space_for_task..."

def monitorstrjustify(strtojustify, flagtime=False):
	justifylimit = 32
	if flagtime:
		justifylimit = 40

	if len(strtojustify) < justifylimit:
		return strtojustify + (" " * (justifylimit - len(strtojustify))) + "|"
	else:
		return strtojustify[0:justifylimit] + "|"

def debianimageinredcolor():
	reddebianlogo = list()
	for simpleline in debianimage:
		reddebianlogo.append(stringwithcolor(simpleline, "RED"))
	return reddebianlogo

def unifydebianlogoandtoolmessage():
	mix, debianwithcolor = list(), debianimageinredcolor()
	flagc, whitechar = marktoreplacestr(), especialwhiteline()
	addheader, totasks, i = True, 0, 0
	
	while i < len(debianwithcolor) or i < len(toolmessage):
		if i < len(debianwithcolor) and i < len(toolmessage):
			mix.append(debianwithcolor[i] + toolmessage[i])
		elif i < len(debianwithcolor):
			if not addheader:
				mix.append(debianwithcolor[i] + flagc)
				totasks += 1
			else:
				mix.append(debianwithcolor[i] + headeractivetaskmonitor())
				addheader = False
		elif i < len(toolmessage):
			mix.append(whitechar + toolmessage[i])
		else:
			break
		i += 1

	return mix, totasks

def graphicparsetask(task):
	taskname, taskdatetime = next(iter(task.items()))
	taskdate, numerictimes = next(iter(taskdatetime.items()))
	hours, minutes, seconds = numerictimes[0], numerictimes[1], numerictimes[2]

	taskdict = { 
		monitorstrjustify(taskname): "GREEN", 
		monitorstrjustify(taskdate): "GREEN"
	}
			
	timediff = strcoloredmapasstr({"{hours} ".format(hours=hours): "GREEN"})
	timediff += strcoloredmapasstr({"hours: ": "RED"})
	timediff += strcoloredmapasstr({"{minutes} ".format(minutes=minutes): "GREEN"})
	timediff += strcoloredmapasstr({"minutes: ": "BLUE"})
	timediff += strcoloredmapasstr({"{seconds} ".format(seconds=seconds): "GREEN"})
	timediff += strcoloredmapasstr({"seconds: ": "YELLOW"})

	if not ( ("finished" in hours) or ("finished" in minutes) or ("finished" in seconds) ):
		return strcoloredmapasstr(taskdict) + timediff
	else:
		return Back.YELLOW + strcoloredmapasstr(taskdict) + timediff + Style.RESET_ALL

def addTasksToMonitorReport(coloredtasks):
	report, freefornewtasks = unifydebianlogoandtoolmessage()
	flagc, whitechar = marktoreplacestr(), especialwhiteline()
	limitreport, limittasks = len(report), len(coloredtasks)
	indxreport, indxtasks = (len(report) - freefornewtasks), 0

	while indxtasks < limittasks:
		if not (indxreport < limitreport):
			report.append(whitechar+coloredtasks[indxtasks])		
		else:
			report[indxreport] = report[indxreport].replace(flagc, coloredtasks[indxtasks], 1)
			indxreport += 1
		indxtasks += 1

	return report

def cleanPreviousTasksStatus(totalprevtasks):
	vlcoutputlinesdebian = 10
	aproxlinestodelete = totalprevtasks + vlcoutputlinesdebian
	for i in range(aproxlinestodelete):
		sys.stdout.write("\x1b[1A\x1b[2K")		

def writeTasksStatus(taskstomonitor):
	writtenlines = 0
	for task in taskstomonitor:
		sys.stdout.write(task + "\n")
		writtenlines += 1
	return writtenlines
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         End Section related with GRAPHICS                               
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         Begin Section related with tasks                               
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
import datetime


def validatetaskdatetime(datetimestr):
	format_str = '%Y-%m-%d %H:%M:%S' # The format
	try:
		return datetime.datetime.strptime(datetimestr, format_str)
	except Exception as e:
		print (datetimestr, "is not functional with the datetime format:", format_str, "error:", e)
		return None

def chargehumantasks():
	lines = open('tasks.csv', 'r').readlines()
	tasks = list()

	for line in lines: 
		line = line.strip()
		if not "TASKS|ENDDATE" in line:
			kvtask = line.split("|")
			tasks.append({kvtask[0] : kvtask[1]})
	    
	return tasks

def extractdiffstofinish():
	tasksdifflist, finishedtasks = list(), list()
	tasks = chargehumantasks()

	for task in tasks:
		for k,v in task.items():
			parsermap = {}
			dttobject = validatetaskdatetime(v)
			if dttobject:
				difftofinish = dttobject - datetime.datetime.now()
				strhms = str(difftofinish).split(".")[0].split(":")
				if ("-" in strhms[0]) or ("-" in strhms[1]) or ("-" in strhms[2]):
					parsermap[v] = ["finished", "finished", "finished"]
					finishedtasks.append({k:v})
				else:
					parsermap[v] = [strhms[0], strhms[1], strhms[2]]

				tasksdifflist.append({k:parsermap})
			else:
				print("Ommiting task:", k, v, "cause:")
				print("tasks with invalid datetime are not valid for monitor")

	return tasksdifflist, finishedtasks

def obtaintaskstomonitor():
	taskswithdiffs, finishedtasks = extractdiffstofinish()
	taskstomonitor = list()
		
	for task in taskswithdiffs:
		taskstomonitor.append(graphicparsetask(task))

	return taskstomonitor, finishedtasks
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         End Section related with tasks                       
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------

#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         Begin Section related with sound                       
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
async def startlittlealertsound(tasks):
	sys.stdout.write(messageToDeleteRing() + "\n")
	os.system("cvlc --play-and-exit --no-repeat classicring.mp3")
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------
#                                         End Section related with sound                       
#----------------------------------------------------------------------------------------------
#----------------------------------------------------------------------------------------------

# Launch and starter final section:(MAX 80 Code LINES)
import os
import asyncio

async def monitor():
	taskstomonitor, finishedtasks = obtaintaskstomonitor()
	monitorbase = addTasksToMonitorReport(taskstomonitor)
	writtenlines = writeTasksStatus(monitorbase)
	await asyncio.sleep(1)
	cleanPreviousTasksStatus(writtenlines)
	return finishedtasks

async def alarmcontroller(ring, tasks):
	if ring == 0:
		if tasks:
			main = asyncio.ensure_future(startlittlealertsound(tasks))
		return 1
	elif ring == 10:
		return 0
	else:
		return (ring + 1)

async def main():
	ring = 0
	while True:
		tasks = await monitor()
		ring = await alarmcontroller(ring, tasks)
			
main = asyncio.ensure_future(main())
loop = asyncio.get_event_loop()
loop.run_forever()